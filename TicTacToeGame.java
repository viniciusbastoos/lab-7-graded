import java.util.Scanner;
public class TicTacToeGame{
    public static void main(String[] args){
        System.out.println("Welcome to the game!");
        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
    
        System.out.println("Player 1's token: X");
        System.out.println("Players 2's token: O");
        
        //#4
        while(!gameOver) {
            //a.
            System.out.println(board);

            //b.
            if (player == 1){
                playerToken = Square.X;
            } 
            else{
                playerToken = Square.O;
            }

            //c.
            Scanner scanner = new Scanner(System.in);

            System.out.print("Enter the row number: ");
            int row = scanner.nextInt();

            System.out.print("Enter the column number: ");
            int column = scanner.nextInt();

            //d.
            while(!board.placeToken(row, column, playerToken)){
                System.out.println("Invalid move. Try again.");

                System.out.print("Enter the row number: ");
                row = scanner.nextInt();

                System.out.print("Enter the column number: ");
                column = scanner.nextInt();
            }

            //e.
            //  i.
            if (board.checkIfFull()){
                System.out.println("It's a tie!");
                gameOver = true;
            } 

            //  ii.
            else if (board.checkIfWinning(playerToken)){
                System.out.println("Player " + player + " is the winner!");
                gameOver = true;
            } 

            //  iii
            else{
                player++;

                if (player > 2){
                    player = 1;
                }
            }
        }
    
    
    }
}