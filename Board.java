public class Board{
    // #1 
    /*A private field that is of type Square[][] called tictactoeBoard*/
    private Square[][] tictactoeBoard =new Square[3][3];

    // #2 
    /* A constructor that takes no inputs and initializes the Square[][] field 
    to a 3 x 3 array. Each Square inside the 2d array should be initialized to BLANK.*/
    public Board(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

    // #3
    /*Override the toString() method that returns a String representation of board. 
    The board should printed like a table where each “row” is printed on a new line 
    (so you’ll need to use nested loops to make a multi-line String!)*/
    public String toString() {
        String result = "0 1 2";
        result = result + "\n";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                result += tictactoeBoard[i][j] + " ";
            }
            result = result + i + "\n";
        }
        return result;
    }

    // #4
    /*Add an instance method called, placeToken(int row, int col, Square playerToken) 
    that takes three input parameters and returns a boolean value. The two integers 
    represent the row and column indexes and a Square parameter representing the token 
    on the player (X or O). This method should check if the given row and column value is valid. 
        a. To do so, first check if the row and column values are in the correct range for a 3 x 3 board. 
            If not, return false. 
        b. Then check the tictactoeBoard array at the given row and column value array has 
            a value equal to BLANK. If yes, set this position to be equal to the input Square 
            parameter and return true. If not, return false.*/
    public boolean placeToken(int row, int col, Square playerToken) {
        // check range 
        if (row >= 0 && row < 3 && col >= 0 && col < 3) {
            // check if position is blank
            if (tictactoeBoard[row][col] == Square.BLANK) {
                // set position to player token
                tictactoeBoard[row][col] = playerToken;
                return true;
            }
        }
        return false;
    }

    // #5 
    /*Add an instance method called checkIfFull that takes as no inputs and returns false, 
    if the Square[][] has at least 1 BLANK value*/
    public boolean checkIfFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tictactoeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    // #6
    /*Add a private method called checkIfWinningHorizontal(Square playerToken) which checks if 
    all the squares in a single row of  the Square[][] array is equal to the playerToken. If yes, 
    return true.*/
    private boolean checkIfWinningHorizontal(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[i][0] == playerToken && 
                tictactoeBoard[i][1] == playerToken && 
                tictactoeBoard[i][2] == playerToken) {
                    return true;
            }
        }
        return false;
    }

    // #7
    /*Add a second private method called checkiIWinningfVertical(Square playerToken) which 
    checks if all the squares in one column are equal to playerToken. If yes, return true.*/
    private boolean checkIfWinningVertical(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[0][i] == playerToken && 
                tictactoeBoard[1][i] == playerToken && 
                tictactoeBoard[2][i] == playerToken) {
                    return true;
            }
        }
        return false;
    }

    // #8
    /*Add an instance method checkIfWinning(Square playerToken) that uses the two private methods 
    that you just defined to check if the player won. Within this method, write an if block to 
    checkIfWinningHorizontal or checkifWinningVertical returns true. 
    If yes, checkIfWinning should return true.*/
    public boolean checkIfWinning(Square playerToken) {
        if (checkIfWinningHorizontal(playerToken) ||
            checkIfWinningVertical(playerToken) ||
            checkIfWinningDiagonal(playerToken)) {
                return true;
        }
        return false;
    }

    // #9 (bonus)
    /*Add a checkIfWinningDiagonal(Square playerToken) method that checks the 2 diagonals.*/
    private boolean checkIfWinningDiagonal(Square playerToken) {
        if (
            tictactoeBoard[0][0] == playerToken && 
            tictactoeBoard[1][1] == playerToken && 
            tictactoeBoard[2][2] == playerToken) {
                return true;
        } 
        else if (tictactoeBoard[2][0] == playerToken && 
            tictactoeBoard[1][1] == playerToken && 
            tictactoeBoard[0][2] == playerToken) {
                return true;
        }
        return false;
    }
}